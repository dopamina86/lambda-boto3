# from functools import wraps
import boto3 # import library to use AWS API
import os # import library to use the local OS commands 
import sys # import library to access variables provided by the user
import datetime # import library to use date

# Global VARs for S3 Bucket name and the folder to upload the Lambdas
try:
    global_bucket_name = sys.argv[1] # Use the 1th argument provide by user as Bucket name
except IndexError:
    print("You must provide the Bucket name as 1th argument")
    sys.exit()
try:
    global_bucket_folder = sys.argv[2] + "/" # Concat the name of the Bucket Folder give it for the user at the invocation of the python scipt + an /
except IndexError:
    print("You must provide the Bucket Folder for Lambda stores as 2nd argument")
    sys.exit()
try:
    global_path = sys.argv[3]
    os.chdir(global_path) # Change the directory where the script will run
except IndexError:
    global_path = os.getcwd()
    pass # If it's not provided an directory, the scrip use the one where the script is invoked
except FileNotFoundError: # If the directory provided is wrong
    print("The 3rd argument is optional. Specify the local directory of the ZIP Lambda files")
    sys.exit()
log = open(global_path+'/log.txt', 'a')
log.write("\n" + "=" * 90 + "\n***SCRIPT STARTED AT: " + str(datetime.datetime.now()) + "\n" + "=" * 90 + "\n")

# Function that creates a list of ZIP files in the current directory
def list_zip_files(path=None):
    files = os.listdir(path)
    files_zip = []
    count = 0

    for items in files:
        if items.endswith(".zip"):
            files_zip.append(items)
            count += 1
    if count == 0:
        message = "There is not ZIP file in the current directory!\n"
        print(message)
        log.write(message)
        log.write("\n" + "=" * 90 + "\n*** SCRIPT FINISHED AT: " + str(datetime.datetime.now()) + "\n" + "=" * 90 + "\n")
        log.close()
        sys.exit()
    else:
        return files_zip

# Function to validate if update the S3 bucket or not
def update_bucket_validation(bucket_name):
    update_bucket_question = input("Update Lambdas ZIP to bucket '" + bucket_name + "' (y/[N])? : ").lower() or "n"

    while True:
        if update_bucket_question != "y" and update_bucket_question != "n":
            print("The only values allowed are 'Y' or 'N': ")
            update_bucket_question = input("Update Lambdas ZIP to bucket '" + bucket_name + "' (y/[N])? : ").lower() or "n"
        else:
            if update_bucket_question == "y":
                update_bucket = True
            elif update_bucket_question == "n":
                update_bucket = False
            break
    
    return update_bucket

# Function to update the S3 Bucket with the Lambda ZIP file
def update_bucket(name_zip, bucket_name, bucket_folder):
    name = name_zip.replace(".zip", "")
    message = "\n" + "=" * 90 + "\n" + "Lambda name: " + name + "\n" + "Lambda zip: " + name_zip + " \n"
    print(message)
    log.write(message)

    client = boto3.resource('s3')
    client.Bucket(bucket_name).upload_file(name_zip, bucket_folder+name_zip)

# Function to validate if the Dry Run mode for Lambda code update will be active or not
def dry_run_validation():
    dry_run_question = input("Update Lambdas code in Dry Run mode ([Y]/n) ?: ").lower() or "y"

    while True:
        if dry_run_question != "y" and dry_run_question != "n":
            print("The only values allowed are 'Y' or 'N': ")
            dry_run_question = input("Update Lambdas code in Dry Run mode ([Y]/n) ?: ").lower() or "y"
        else:
            if dry_run_question == "y":
                dry_run = True
            elif dry_run_question == "n":
                dry_run = False
            break
    
    return dry_run

# Function to update the Lambda Function code with the ZIP file inside a S3 Bucket
def update_lambda_zip(name_zip, bucket_name, bucket_folder, dry_run=True):
    name = name_zip.replace(".zip", "")
    message = "\n" + "=" * 90 + "\nLambda name: " + name + "\nLambda zip: " + name_zip + "\n"
    print(message)
    log.write(message)

    client = boto3.client('lambda')
    try:
        response = client.update_function_code(
            FunctionName=name,
            S3Bucket=bucket_name,
            S3Key=bucket_folder+name_zip,
            DryRun=dry_run,
        )
    except:
        response = "*** ERROR: An error happend updating the Lambda Function '" + bucket_folder+name_zip + "'. Is there in the S3 Bucket? ***"

    return response

def main ():
    lambda_names = list_zip_files() # List the current directory filtering only the ZIP files
    bucket_name = global_bucket_name
    bucket_folder = global_bucket_folder

    # Verify if the user wants update the bucket with the Lambdas ZIP in the current directory
    update_bucket_question = update_bucket_validation(bucket_name)
    message = "\nUpdate Bucket: " + str(update_bucket_question) + " \n"
    print(message)
    log.write(message)

    # If the user wants to update the bucket, just do it
    if update_bucket_question:
        message = "=" * 90 + "\nUpdate S3 Bucket Proccess Started\n" + "=" * 90 + "\n"
        print(message)
        log.write(message)
        for name_zip in lambda_names:
            update_bucket(name_zip, bucket_name, bucket_folder)
        message = "=" * 90 + "\nUpdate S3 Bucket Proccess Finished\n" + "=" * 90
        print(message)
        log.write(message)

        message = "\n" + "=" * 90 + "\nUpdate Bucket: " + str(update_bucket_question) + "\n"
        print(message)
        log.write(message)

    # Verify if the user wants to actually update the Lambda code or just make a Dry Run
    dry_run = dry_run_validation()
    message = "\nUpdate Lambdas code in Dry Run mode: " + str(dry_run) + "\n"
    print(message)
    log.write(message)

    message = "=" * 90 + "\nUpdate Lambda Proccess Started\n" + "=" * 90
    print(message)
    log.write(message)

    # Update the Lambdas Code. The Dry Run mode is provided as argument
    for name_zip in lambda_names:
        response = update_lambda_zip(name_zip, bucket_name, bucket_folder, dry_run)
        if isinstance(response, str):
            print(response)
            log.write(response)
        else:
            for key in response:
                message = key + ": " + str(response[key]) + "\n"
                print(message)
                log.write(message)
    message = "\n" * 2 + "=" * 90 + "\nUpdate Lambda Proccess Finished\n" + "=" * 90
    print(message)
    log.write(message)
    message = "\n" + "=" * 90 + "\nUpdate Lambdas code in Dry Run mode: " + str(dry_run) + "\n"
    print(message)
    log.write(message)

    log.write("\n" + "=" * 90 + "\n*** SCRIPT FINISHED AT: " + str(datetime.datetime.now()) + "\n" + "=" * 90 + "\n")
    log.close()

if __name__ == "__main__":
    main()