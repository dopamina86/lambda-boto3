# from functools import wraps
import boto3 # import library to use AWS API
import os # import library to use the local OS commands 
import sys # import library to access variables provided by the user
import datetime # import library to use date

# Global VARs for S3 Bucket name and the folder to upload the Lambdas
try:
    global_bucket_name = sys.argv[1] # Use the 1th argument provide by user as Bucket name
except IndexError:
    print("You must provide the Bucket name as 1th argument")
    sys.exit()
try:
    global_bucket_folder = sys.argv[2] + "/" # Concat the name of the Bucket Folder give it for the user at the invocation of the python scipt + an /
except IndexError:
    print("You must provide the Bucket Folder for Lambda stores as 2nd argument")
    sys.exit()
try:
    global_arn_role = sys.argv[3]
except IndexError:
    print("You must provide the ARN Role for Lambdas as 3rd argument")
    sys.exit()
try:
    global_subnet_ids = sys.argv[4].split(",")
except IndexError:
    print("You must provide the Global Subnet IDs  for Lambdas as 4th argument: subnet_id_A,subnet_id_B")
    sys.exit()
try:
    global_security_group_ids = sys.argv[5].split(",")
except IndexError:
    print("You must provide the Global SG IDs  for Lambdas as 5th argument: sg_id_01,sg_id_02")
    sys.exit()
try:
    global_runtime = sys.argv[6]
except IndexError:
    print("You must provide the Global Runtime for Lambdas as 6th argument")
    sys.exit()
try:
    global_project_tag = sys.argv[7]
except IndexError:
    print("You must provide the Project tag as 7th argument")
    sys.exit()

global_path = os.getcwd()
log = open(global_path+'/log.txt', 'a')
log.write("\n" + "=" * 90 + "\n***SCRIPT STARTED AT: " + str(datetime.datetime.now()) + "\n" + "=" * 90 + "\n")

# Function that creates a list of ZIP files in the current directory
def list_zip_files(path=None):
    files = os.listdir(path)
    files_zip = []
    count = 0

    for items in files:
        if items.endswith(".zip"):
            files_zip.append(items)
            count += 1
    if count == 0:
        message = "There is not ZIP file in the current directory!\n"
        print(message)
        log.write(message)
        log.write("\n" + "=" * 90 + "\n*** SCRIPT FINISHED AT: " + str(datetime.datetime.now()) + "\n" + "=" * 90 + "\n")
        log.close()
        sys.exit()
    else:
        return files_zip

# Function to validate if update the S3 bucket or not
def update_bucket_validation(bucket_name):
    update_bucket_question = input("Update Lambdas ZIP to bucket '" + bucket_name + "' (y/[N])? : ").lower() or "n"

    while True:
        if update_bucket_question != "y" and update_bucket_question != "n":
            print("The only values allowed are 'Y' or 'N': ")
            update_bucket_question = input("Update Lambdas ZIP to bucket '" + bucket_name + "' (y/[N])? : ").lower() or "n"
        else:
            if update_bucket_question == "y":
                update_bucket = True
            elif update_bucket_question == "n":
                update_bucket = False
            break
    
    return update_bucket

# Function to update the S3 Bucket with the Lambda ZIP file
def update_bucket(name_zip, bucket_name, bucket_folder):
    name = name_zip.replace(".zip", "")
    message = "\n" + "=" * 90 + "\n" + "Lambda name: " + name + "\n" + "Lambda zip: " + name_zip + " \n"
    print(message)
    log.write(message)

    client = boto3.resource('s3')
    client.Bucket(bucket_name).upload_file(name_zip, bucket_folder+name_zip)

# Function to validate if the Dry Run mode for Lambda code update will be active or not
def dry_run_validation():
    dry_run_question = input("Update Lambdas code in Dry Run mode ([Y]/n) ?: ").lower() or "y"

    while True:
        if dry_run_question != "y" and dry_run_question != "n":
            print("The only values allowed are 'Y' or 'N': ")
            dry_run_question = input("Update Lambdas code in Dry Run mode ([Y]/n) ?: ").lower() or "y"
        else:
            if dry_run_question == "y":
                dry_run = True
            elif dry_run_question == "n":
                dry_run = False
            break
    
    return dry_run

# Function to create the Lambda Function code with the ZIP file inside a S3 Bucket
def create_lambda_zip(name_zip, bucket_name, bucket_folder, arn_role, runtime, project_tag, subnet_ids, security_group_ids, handler="index.handler", mem_size=128, time_out=3):
    name = name_zip.replace(".zip", "")
    message = "\n" + "=" * 90 + "\nLambda name: " + name + "\nLambda zip: " + name_zip + "\n"
    print(message)
    log.write(message)

    client = boto3.client('lambda')
    try:
        response = client.create_function(
            Code={
                'S3Bucket': bucket_name,
                'S3Key': bucket_folder+name_zip,
            },
            # Description='',
            # Environment={
            #     'Variables': {
            #         'BUCKET': 'my-bucket-1xpuxmplzrlbh',
            #         'PREFIX': 'inbound',
            #     },
            # },
            FunctionName=name,
            Handler=handler,
            # KMSKeyArn='arn:aws:kms:us-west-2:123456789012:key/b0844d6c-xmpl-4463-97a4-d49f50839966',
            MemorySize=mem_size,
            Publish=True,
            Role=arn_role,
            Runtime=runtime,
            Tags={
                'Project': project_tag,
            },
            Timeout=time_out,
            # TracingConfig={
            #     'Mode': 'Active',
            # },
            VpcConfig={
                'SubnetIds': subnet_ids,
                'SecurityGroupIds': security_group_ids,
                # 'VpcId': vpc_id
            }
        )
    except:
        response = "*** ERROR: An error happend creating the Lambda Function '" + bucket_folder+name_zip + "' ***"

    return response

def main ():
    lambda_names        = list_zip_files() # List the current directory filtering only the ZIP files
    bucket_name         = global_bucket_name
    bucket_folder       = global_bucket_folder
    arn_role            = global_arn_role
    runtime             = global_runtime
    project_tag         = global_project_tag
    subnet_ids          = global_subnet_ids
    security_group_ids  = global_security_group_ids

    # Verify if the user wants update the bucket with the Lambdas ZIP in the current directory
    update_bucket_question = update_bucket_validation(bucket_name)
    message = "\nUpdate Bucket: " + str(update_bucket_question) + " \n"
    print(message)
    log.write(message)

    # If the user wants to update the bucket, just do it
    if update_bucket_question:
        message = "=" * 90 + "\nUpdate S3 Bucket Proccess Started\n" + "=" * 90 + "\n"
        print(message)
        log.write(message)
        for name_zip in lambda_names:
            update_bucket(name_zip, bucket_name, bucket_folder)
        message = "=" * 90 + "\nUpdate S3 Bucket Proccess Finished\n" + "=" * 90
        print(message)
        log.write(message)

        message = "\n" + "=" * 90 + "\nUpdate Bucket: " + str(update_bucket_question) + "\n"
        print(message)
        log.write(message)

    message = "=" * 90 + "\nCreate Lambda Proccess Started\n" + "=" * 90
    print(message)
    log.write(message)

    # Create the Lambdas
    for name_zip in lambda_names:
        response = create_lambda_zip(name_zip, bucket_name, bucket_folder, arn_role, runtime, project_tag, subnet_ids, security_group_ids)
        if isinstance(response, str):
            print(response)
            log.write(response)
        else:
            for key in response:
                message = key + ": " + str(response[key]) + "\n"
                print(message)
                log.write(message)
    message = "\n" * 2 + "=" * 90 + "\nCreate Lambda Proccess Finished\n" + "=" * 90
    print(message)
    log.write(message)

    log.write("\n" + "=" * 90 + "\n*** SCRIPT FINISHED AT: " + str(datetime.datetime.now()) + "\n" + "=" * 90 + "\n")
    log.close()

if __name__ == "__main__":
    main()